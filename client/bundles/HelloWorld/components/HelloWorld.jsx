import PropTypes from 'prop-types';
import React, { useState } from 'react';
import style from './HelloWorld.module.css';

const HelloWorld = (props) => {
  const [name, setName] = useState(props.name);

  return (
    <div className="max-w-xl mx-auto py-12 md:max-w-4xl">
      <h3 className="text-green-400 text-3xl">Hello, {name}!</h3>
      <hr />
      <form>
        <div className="mt-8 grid grid-cols-1 md:grid-cols-2 gap-6 items-start">
          <div className="grid grid-cols-1 gap-6">
            <label className="block" htmlFor='name'>
              <span className="text-gray-700">Say Hello To...</span>
              <input id="name" type="text" className="form-input mt-1 block w-full rounded-sm" placeholder={name} onChange={(e) => setName(e.target.value)}></input>
            </label>
          </div>
        </div>
      </form>
    </div>
  );
};

HelloWorld.propTypes = {
  name: PropTypes.string.isRequired, // this is passed from the Rails view
};

export default HelloWorld;
